module.exports = function(io){
  io.sockets.on('connection', function (socket) {
    // when the client emits 'adduser', this listens and executes
    socket.on('addusertoroom', function(room){
      console.log("user added to " + room);
      // store the room name in the socket session for this client
      socket.room = room;
      // send client to room 1
      socket.join(room);
      // echo to client they've connected
      socket.emit('updateroom', 'SERVER', 'you have connected to room:' + room);
      // echo to room 1 that a person has connected to their room
      socket.broadcast.to(room).emit('updateroom', 'SERVER', 'new user has connected to this room');
    });

    // when the client emits 'sendchat', this listens and executes
    socket.on('sendline', function (data) {
      // we tell the client to execute 'updatechat' with 2 parameters
      console.log("sending line");
      io.sockets.in(socket.room).emit('emitline', data);
    });

    // when the user disconnects.. perform this
    socket.on('disconnect', function(){
      // echo globally that this client has left
      socket.broadcast.emit('updateroom', 'SERVER', 'user has disconnected');
      socket.leave(socket.room);
    });
  });
}